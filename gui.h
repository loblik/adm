#ifndef _GUI_H
#define _GUI_H

#include <GL/gl.h>
#include "mat.h"

#define CURSOR_RADIUS	0.04

struct canvas;

enum canvas_button { CANVAS_BTN_LEFT, CANVAS_BTN_RIGHT };
enum canvas_button_action { CANVAS_BTN_UP, CANVAS_BTN_DOWN };

struct canvas {
	int mouse_x;
	int mouse_y;
	int offset_x;
	int offset_y;

	int width;
	int height;
	int win_width;
	int win_height;

	int gsize;
	int btn_left_pressed;

	int cursor_radius;

	unsigned char *buffer;
	float *buffer_input;

	struct mat matrix;
	int render;
};

void canvas_cursos_size_inc(struct canvas *c);
void canvas_cursos_size_dec(struct canvas *c);

void canvas_cursor_move(struct canvas *c, int x, int y);
void canvas_mouse_click(struct canvas*, enum canvas_button, enum canvas_button_action);
int canvas_init(struct canvas *c, unsigned width, unsigned height,
				 unsigned offset_x, unsigned offset_y, int gsize);

void canvas_clipping_planes(struct canvas *c, int *left, int *top,
							int *right, int *bottom);

int canvas_right_update(struct canvas *c);
void canvas_render(struct canvas*);
void canvas_resize(struct canvas*, int w, int h);
void canvas_clear(struct canvas*);
void canvas_save(struct canvas *c);

const struct mat* canvas_get_matrix(struct canvas *c);
void canvas_set_matrix(struct canvas *c, const struct mat *mat);
#endif
