#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include "mat.h"
#include "options.h"
#include "keymap.h"
#include "gui.h"
#include "dataset.h"
#include "adm.h"

#define CANVAS_SIZE		800
#define GRID_SIZE		30
#define OUTPUT_SIZE		10

void glfw_close(GLFWwindow *win);
void glfw_mouse_button(GLFWwindow *win, int button, int action, int keys);
void glfw_keyboard(GLFWwindow *win, int key, int code, int action, int mods);
void glfw_cursor_move(GLFWwindow *win, double x, double y);
void glfw_resize(GLFWwindow *win, int w, int h);
void glfw_refresh(GLFWwindow *win);

static void adm_show_samples_cb(const struct mat *in, const struct mat *out, void *adm) {
	struct adm *app = (struct adm*)adm;
	canvas_set_matrix(&app->cv, in);
	app->cv.render = 1;
	canvas_render(&app->cv);
	glfwSwapBuffers(app->win);
}

void glfw_close(GLFWwindow *win) {
	printf("glfw closing\n");
	struct adm *app = glfwGetWindowUserPointer(win);
	FILE *out;
	if ((out = fopen(app->opts.file, "w+"))) {
		data_set_write(&app->data, out);
	}
	key_map_free(&app->kmap);
	data_set_free(&app->data);
}

void glfw_mouse_button(GLFWwindow *win, int button, int action, int keys) {
	if ((action == GLFW_RELEASE || action == GLFW_PRESS) &&
		 (button == GLFW_MOUSE_BUTTON_LEFT || button == GLFW_MOUSE_BUTTON_RIGHT)) {
		struct adm *app = glfwGetWindowUserPointer(win);
		struct canvas *c = &app->cv;
		enum canvas_button canvas_button;
		enum canvas_button_action canvas_button_action;

		if (action == GLFW_RELEASE) {
			canvas_button_action = CANVAS_BTN_UP;
			canvas_right_update(&app->cv);
			const struct mat *input = canvas_get_matrix(&app->cv);
			net_propagate(&app->nn, input);
		} else {
			canvas_button_action = CANVAS_BTN_DOWN;
		}
		if (button == GLFW_MOUSE_BUTTON_LEFT) {
			canvas_button = CANVAS_BTN_LEFT;
		} else {
			canvas_button = CANVAS_BTN_RIGHT;
		}
		canvas_mouse_click(c, canvas_button, canvas_button_action);
	}
}

void glfw_refresh(GLFWwindow *win) {
	struct adm *app = glfwGetWindowUserPointer(win);
	struct canvas *c = &app->cv;
	c->render = 1;
}

void glfw_resize(GLFWwindow *win, int w, int h) {
	struct adm *app  = glfwGetWindowUserPointer(win);
	struct canvas *c = &app->cv;
	canvas_resize(c, w, h);
}

void glfw_cursor_move(GLFWwindow *win, double x, double y) {
	struct adm *app = glfwGetWindowUserPointer(win);
	canvas_cursor_move(&app->cv, (int)x, (int)y);
}

void glfw_keyboard(GLFWwindow *win, int key, int code, int action, int mods) {
	struct adm *app = glfwGetWindowUserPointer(win);

	if (action == GLFW_PRESS) {
#ifdef _DEBUG
		printf("press %c %d\n", key, (int)key);
#endif
		char first = key_map_get_first(&app->kmap);
		char last = key_map_get_last(&app->kmap);
		if (mods & GLFW_MOD_SHIFT) {
			switch(key) {
				case '=':
					canvas_cursos_size_inc(&app->cv); break;
				case '-':
					canvas_cursos_size_dec(&app->cv); break;
			}
		} else if (key >= first && key <= last) {
			const struct mat *target = key_map_target(&app->kmap, key);
			data_set_add(&app->data, canvas_get_matrix(&app->cv),target);
		} else {
			switch(key) {
					canvas_cursos_size_dec(&app->cv); break;
				case 'P':
					data_set_print(&app->data);
					break;
				case 'S':
					data_set_for_each(&app->data, &adm_show_samples_cb, (void*)app);
				case 'C':
					canvas_clear(&app->cv);
					break;
				case 'Q':
					glfw_close(win);
					glfwSetWindowShouldClose(win, GL_TRUE);
			}
		}
	}
}

int main(int argc, char **argv) {

	FILE *file;

	struct adm app;

	key_map_init(&app.kmap, OUTPUT_SIZE, '0');
	options_parse(&app.opts, argc, argv);

	app.opts.input_size = GRID_SIZE*GRID_SIZE;
	app.opts.output_size = OUTPUT_SIZE;

	file = fopen(app.opts.file, "r");
	if (!file) {
		data_set_init(&app.data, app.opts.input_size, app.opts.output_size);
	} else {
		data_set_read(&app.data, file);
	}
	srand(time(NULL));


    if (!glfwInit())
		return 0;
    app.win = glfwCreateWindow(800, 400,
				"Digit recognition - MI-ADM course project", NULL, NULL);
    if (!app.win) {
        glfwTerminate();
        return 0;
    }

	if (canvas_init(&app.cv, 330, 330, 30, 30, GRID_SIZE) == 0) {
		printf("cannot initialize gui\n");
		exit(EXIT_FAILURE);
	}

/*	struct net n;
	c.user_data = &n; */
	net_init(&app.nn, GRID_SIZE*GRID_SIZE, 3, OUTPUT_SIZE);

    glfwMakeContextCurrent(app.win);
    glfwSetKeyCallback(app.win, glfw_keyboard);
    glfwSetWindowSizeCallback(app.win, glfw_resize);
    glfwSetWindowUserPointer(app.win, (void*)&app);
    glfwSetCursorPosCallback(app.win, glfw_cursor_move);
    glfwSetMouseButtonCallback(app.win, glfw_mouse_button);
    glfwSetWindowCloseCallback(app.win, glfw_close);
    glfwSetWindowRefreshCallback(app.win, glfw_refresh);
    int win_width, win_height;
    glfwGetFramebufferSize(app.win, &win_width, &win_height);
    canvas_resize(&app.cv, win_width, win_height);

    glClearColor(.4, .45, .15, 0);
    while (!glfwWindowShouldClose(app.win)) {
		if (app.cv.render) {
			app.cv.render = 0;
			glClear(GL_COLOR_BUFFER_BIT);
			canvas_render(&app.cv);
	        glfwSwapBuffers(app.win);
		}
	    glfwWaitEvents();
    }
    glfwTerminate();
    return 0;
}
