CC=gcc
CFLAGS=-Wall -Werror
LDFLAGS=-lX11 -lGLU -lGL -lm -lX11 -lGL -lglfw3 -lXxf86vm -lrt -lpthread -lXrandr -lXi

all: adm

debug: CFLAGS += -D_DEBUG -g
debug: adm

adm: mat.o options.o gui.o adm.o keymap.o dataset.o nn.o
	$(CC) nn.o gui.o adm.o mat.o options.o keymap.o dataset.o -o adm $(LDFLAGS)

adm.o: adm.c adm.h
	$(CC) $(CFLAGS) -c adm.c

gui.o: gui.c gui.h
	$(CC) $(CFLAGS) -c gui.c

mat.o: mat.c mat.h
	$(CC) $(CFLAGS) -c mat.c

options.o: options.c options.h
	$(CC) $(CFLAGS) -c options.c

keymap.o: keymap.c keymap.h
	$(CC) $(CFLAGS) -c keymap.c

dataset.o: dataset.c dataset.h
	$(CC) $(CFLAGS) -c dataset.c

nn.o: nn.c nn.h
	$(CC) $(CFLAGS) -c nn.c

clean:
	rm -rf *o adm
