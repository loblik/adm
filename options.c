#include <errno.h>
#include <limits.h>
#include <stdlib.h>
#include <stdio.h>
#include <getopt.h>
#include <unistd.h>
#include <math.h>
#include "options.h"

#define DEFAULT_DATASET_NAME "data.bin"

int options_arg_to_uint(char *arg) {
	char *endptr;
	long val;
	errno = 0;
	val = strtol(arg, &endptr, 10);
	if ((errno == ERANGE && (val == LONG_MAX || val == LONG_MIN))
		|| (errno != 0 && val == 0)
		|| (*endptr != '\0' || endptr == arg)
		|| (val < 1 && val > INT_MAX)) {
		fprintf(stderr, "argument must be an integer in the range 1 - %d\n", INT_MAX);
    	exit(EXIT_FAILURE);
	}
	return (int)val;
}

void options_help() {
    fprintf(stderr, "%s [OPTIONS] DATA-SET\n\n\
options:\n\
  -h	number of nodes in hidden layer\n\
  -o	number of output nodes\n\
  -i	number of input nodes\n\
  -h	print this help\n\n", "mperc");
}

void options_parse(struct options *opts, int argc, char **argv) {
	char c;
	int num_inputs;
	while ((c = getopt (argc, argv, "h:i:o:")) != -1)
		switch (c) {
        	case 'h':
				opts->hidden_size = options_arg_to_uint(optarg);
            	break;
           	case 'i':
				num_inputs = options_arg_to_uint(optarg);
				if (sqrt(num_inputs) != (int)sqrt(num_inputs)) {
					fprintf(stderr, "number of inputs must be perfect square\
									 (because it's a grid/table)");
					exit(EXIT_FAILURE);
				}
				opts->input_size = num_inputs;
            	break;
           	case 'o':
				opts->output_size = options_arg_to_uint(optarg);
            	break;
			case '?':
			default:
				options_help();
				exit(EXIT_FAILURE);
		}

    if (optind + 1 != argc) {
		fprintf(stderr, "data file not specified, using default %s\n", DEFAULT_DATASET_NAME);
		opts->file = DEFAULT_DATASET_NAME;
    } else {
		opts->file = argv[optind];
	}
}
