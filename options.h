#ifndef _ARGS_H
#define _ARGS_H

struct options {
	int hidden_size;
	int input_size;
	int output_size;
	char *file;
};

void options_parse(struct options *opts, int argc, char **argv);

#endif
