# adm #

Simple pattern recognition program written in C as semestral work for algorithms of data mining course. It uses 3-layer neural network which is trained using basic backpropagation algorithm.