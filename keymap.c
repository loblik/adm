#include "keymap.h"
#include "mat.h"
#include <stdlib.h>

char key_map_get_first(struct key_map *map) {
	return map->key_start;
}

char key_map_get_last(struct key_map *map) {
	return map->key_end;
}

void key_map_init(struct key_map *map, int size, char start) {
	int i;
	map->size = size;
	map->key_start = start;
	map->key_end = start + size - 1;
	map->targets = (struct mat*)malloc(sizeof(struct mat)*size);
	for (i = 0; i < size; i++) {
		mat_init(&map->targets[i], size, 1);
		mat_zero(&map->targets[i]);
		mat_set_elem(&map->targets[i], i, 0, 1.0);
	}
}

const struct mat* key_map_target(struct key_map *map, char key) {
	int index = key - map->key_start;
	if (index < 0 || index >= map->size)
		return NULL;
	else
		return &map->targets[index];
}

void key_map_free(struct key_map *map) {
	int i;
	for (i = 0; i < map->size; i++)
		mat_free(&map->targets[i]);
	free(map->targets);
}
