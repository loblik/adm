#include "mat.h"
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

static unsigned int mat_error;

#define ERR_OK	mat_error = MAT_OK
#define ERR_MALLOC mat_error = MAT_ALLOC_FAILED ;fprintf(stderr, "alloc failed\n")
#define ERR_ARG	mat_error = MAT_BAD_ARGUMENT  ;fprintf(stderr, "bad argument\n")
#define ERR_DIMENSION mat_error = MAT_BAD_DIMENSION ;fprintf(stderr, "bad dimension\n")

#define RETURN_IF_NULL(a) if ((a) == NULL) return NULL;

#define MAT_FLAGS_HEAPED	1

struct mat* mat_set_all(struct mat *m, float val, struct mat *res) {
	assert(m != NULL);
	if (res == NULL)
		res = mat_init(NULL, m->rows, m->cols);
	RETURN_IF_NULL(m);

	int i, j;
	for (i = 0; i < m->rows; i++)
		for (j = 0; j < m->cols; j++)
			mat_set_elem(res, i, j, val);
	return res;
}

struct mat* mat_each(struct mat *m, float (*cb)(float), struct mat *res) {
	assert(m != NULL);
	if (res == NULL)
		res = mat_init(NULL, m->rows, m->cols);
	RETURN_IF_NULL(res);

	int i, j;
	for (i = 0; i < m->rows; i++)
		for (j = 0; j < m->cols; j++)
			mat_set_elem(res, i, j, cb(mat_elem(m, i, j)));
	return res;
}

struct mat* mat_zero(struct mat *m) {
	assert(m != NULL);
	int i, j;
	for (i = 0; i < m->rows; i++) {
		for (j = 0; j < m->cols; j++) {
			mat_set_elem(m, i, j, 0.0);
		}
	}
	return m;
}

struct mat* mat_rand(struct mat *m, float min, float max) {
	assert(m != NULL);
	int i, j;
	for (i = 0; i < m->rows; i++)
		for (j = 0; j < m->cols; j++) {
			float rnd = ((float)rand()/RAND_MAX)*(max - min) + min;
			mat_set_elem(m, i, j, rnd);
		}
	return m;
}

static void *sec_malloc(size_t bytes) {
	void *mem = malloc(bytes);
	if (mem == NULL) {
		mat_error = MAT_ALLOC_FAILED;
	}
	return mem;
}

struct mat* mat_trans(struct mat *m, struct mat *res) {
	int allocated = 0;
	assert(m != NULL);
	if (res->cols != m->rows || res->rows != m->cols) {
		ERR_DIMENSION;
		return NULL;
	}
	if (res == NULL)
		res = mat_init(NULL, m->cols, m->rows);
	RETURN_IF_NULL(res);

	if (m == res) {
		m = mat_copy(m, NULL);
		RETURN_IF_NULL(m);
		allocated++;
	}

	int i, j;
	for (i = 0; i < res->rows; i++)
		for (j = 0; j < res->cols; j++) {
			mat_set_elem(res, i, j, mat_elem(m, j, i));
		}
	if (allocated)
		mat_free(m);
	return res;
}

struct mat* mat_sub(struct mat *a, struct mat *b, struct mat *res) {

	assert(a != NULL);
	assert(b != NULL);

	if (a->cols != b->cols || a->rows != a->cols) {
		ERR_DIMENSION;
		return NULL;
	}
	if (res == NULL)
		res = mat_init(NULL, a->rows, a->cols);
	RETURN_IF_NULL(res);

	int i, j;
	for (i = 0; i < a->rows; i++)
		for (j = 0; j < a->cols; j++)
			mat_set_elem(res, i, j, mat_elem(a, i, j) - mat_elem(b, i, j));

	return res;
}

struct mat* mat_add(struct mat *a, struct mat *b, struct mat *res) {
	assert(a != NULL);
	assert(b != NULL);

	if (a->cols != b->cols || a->rows != b->rows) {
		ERR_DIMENSION;
		return NULL;
	}
	if (res == NULL)
		res = mat_init(NULL, a->rows, a->cols);
	RETURN_IF_NULL(res);

	int i, j;
	for (i = 0; i < a->rows; i++)
		for (j = 0; j < a->cols; j++)
			mat_set_elem(res, i, j, mat_elem(a, i, j) + mat_elem(b, i, j));
	return res;
}

float mat_elem(const struct mat *m, unsigned r, unsigned c) {
	assert(r < m->rows && c < m->cols);
	return m->vals[m->cols*r + c];
}

void mat_set_elem(struct mat *m, unsigned r, unsigned c, float val) {
	assert(r < m->rows && c < m->cols);
	m->vals[m->cols*r + c] = val;
	return;
}

struct mat* mat_copy(const struct mat *a, struct mat *res) {
	if (res == NULL)
		res = mat_init(NULL, a->rows, a->cols);
	if (res == NULL)
		return NULL;
	int i, j;
	for (i = 0; i < a->rows; i++)
		for (j = 0; j < a->cols; j++)
			mat_set_elem(res, i, j, mat_elem(a, i, j));

	ERR_OK;
	return res;
}

struct mat* mat_mul(struct mat *a, struct mat *b, struct mat *res) {
	int allocated = 0;
	assert(a != NULL && b != NULL);
	if (a->cols != b->rows) {
		ERR_DIMENSION;
		return NULL;
	}
	if (res == NULL) {
		res = mat_init(NULL, a->rows, b->cols);
	} else if (res->rows != a->rows || res->cols != b->cols) {
		printf("%d ? %d, %d ? %d\n", a->rows, res->rows, res->cols, b->cols);
		ERR_DIMENSION;
		return NULL;
	}
	if (res == NULL)
		return NULL;

	if (res == a) {
		a = mat_copy(a, NULL);
		if (a == NULL)
			return NULL;
		allocated++;
	}
	if (res == b) {
		b = mat_copy(b, NULL);
		if (a == NULL)
			return NULL;
		allocated++;
	}


	int i, j, k;
	for (i = 0; i < res->rows; i++)
		for (j = 0; j < res->cols; j++) {
			float scalar = 0;
			for (k = 0; k < a->cols; k++) {
				scalar += mat_elem(a, i, k) * mat_elem(b, k, j);
			}
			mat_set_elem(res, i, j, scalar);
		}

	if (allocated) {
		mat_free(a);
		allocated--;
	}
	if (allocated)
		mat_free(b);

	ERR_OK;
	return res;
}

void mat_free(struct mat *m) {
	assert(m != NULL);
	free(m->vals);
	if ((m->flags & MAT_FLAGS_HEAPED))
		free(m);
}


struct mat* mat_scalar(struct mat *a, float scalar, struct mat *res) {
	assert(a != NULL);
	if (res == NULL)
		res = mat_init(NULL, a->rows, a->cols);

	if (res == NULL)
		return NULL;

	if (res->cols != a->cols || res->rows != res->rows) {
		ERR_ARG;
		return NULL;
	}

	int i, j;
	for (i = 0; i < a->rows; i++)
		for (j = 0; j < a->cols; j++)
			mat_set_elem(res, i, j, mat_elem(a, i, j) * scalar);

	ERR_OK;
	return res;
}

struct mat* mat_init(struct mat *m, unsigned rows, unsigned cols) {
	if (m == NULL) {
		m = sec_malloc(sizeof(struct mat));
		m->flags = 0 | MAT_FLAGS_HEAPED;
	} else {
		m->flags = 0;
	}
	if (m == NULL)
		return NULL;;

	if (rows == 0 || cols == 0) {
		ERR_ARG;
		return NULL;
	}
	m->vals = (float*)sec_malloc(sizeof(float)*rows*cols);
	if (m->vals == NULL)
		return NULL;

	m->rows = rows;
	m->cols = cols;


	ERR_OK;
	return m;
}

void mat_print(const struct mat *m) {
	int i, j;
	for (i = 0; i < m->rows; i++) {
		for (j = 0; j < m->cols; j++)
			printf("%7.4f ", mat_elem(m, i, j));
		puts("");
	}
	ERR_OK;
	return;
}
