#ifndef ADM_H
#define ADM_H

#include <GLFW/glfw3.h>
#include "dataset.h"
#include "options.h"
#include "keymap.h"
#include "gui.h"
#include "nn.h"

struct adm {
	struct data_set data;
	struct options opts;
	struct key_map kmap;
	struct canvas cv;
	struct net nn;
	GLFWwindow *win;
};

#endif
