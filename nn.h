#ifndef NN_H
#define NN_H
#include "mat.h"

struct net {
	struct {
		struct mat val;
		struct mat val_trans;
	} in;
	struct {
		struct mat weight;
		struct mat val;
		struct mat val_trans;
		struct mat delta;
		struct mat delta_weight;
		struct mat delta_bias;
		struct mat bias;
	} hid;
	struct {
		struct mat weight;
		struct mat val;
		struct mat delta;
		struct mat delta_weight;
		struct mat delta_bias;
		struct mat bias;
	} out;
};

void net_init(struct net *n, int input_size, int hidden_size, int output_size);
void net_backprop(struct net *n, struct mat *target);
const struct mat* net_propagate(struct net *n, const struct mat *input);
#endif
