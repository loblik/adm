#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include "gui.h"

void canvas_cursos_size_inc(struct canvas *c) {
	c->cursor_radius++;
}

void canvas_cursos_size_dec(struct canvas *c) {
	c->cursor_radius--;
}

void canvas_set_matrix(struct canvas *c, const struct mat *mat) {
	int i, size = c->gsize*c->gsize;
	mat_copy(mat, &c->matrix);
	for (i = 0; i < size; i++)
		c->buffer_input[i] = mat_elem(&c->matrix, i, 0);
}

const struct mat* canvas_get_matrix(struct canvas *c) {
	int i, size = c->gsize*c->gsize;
	for (i = 0; i < size; i++)
		mat_set_elem(&c->matrix, i, 0, c->buffer_input[i]);
//	mat_print(&c->matrix);
	return &c->matrix;
}

void canvas_clear(struct canvas *c) {
	memset(c->buffer, 0U, sizeof(unsigned char)*c->width*c->height);
	memset(c->buffer_input, 0U, sizeof(unsigned int)*c->gsize*c->gsize);
	c->render = 1;
}

int canvas_init(struct canvas *c, unsigned width, unsigned height,
				 unsigned offset_x, unsigned offset_y, int gsize) {

	c->cursor_radius = CURSOR_RADIUS*width;
	c->gsize = gsize;
	c->offset_x = offset_x;
	c->offset_y = offset_y;
	c->width = width;
	c->height = height;
	c->btn_left_pressed = 0;
	c->buffer = (unsigned char*)malloc(sizeof(unsigned char)*height*width);
	c->buffer_input = (float*)malloc(sizeof(float)*c->gsize*c->gsize);
	mat_init(&c->matrix, c->gsize*c->gsize, 1);

	canvas_clear(c);
	return 1;
}

void canvas_resize(struct canvas *c, int w, int h) {
	c->win_width = w;
	c->win_height = h;
	glViewport(0, 0, c->win_width, c->win_height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(0.f, c->win_width, 0.f, c->win_height, -1.f, 1.f);
	glMatrixMode(GL_MODELVIEW);
//	c->render = 1;
}

int canvas_square_clip(struct canvas *c, int *x, int *y, int *size) {
	int cl, ct, cr, cb;
	canvas_clipping_planes(c, &cl, &ct, &cr, &cb);
	int height = ct - cb + 1;
	int width = cr - cl + 1;
	if (height <= 0 || width <= 0) {
		*size = 0;
		return 0;
	}
	int diff = abs(height - width);
	if (height > width) {
		*size = ct - cb + 1;
		cl -= diff/2;
	} else {
		*size = cr - cl + 1;
		cb -= diff/2;
	}
	*x = cl;
	*y = cb;
	return 1;
}

int canvas_point_inside(struct canvas *c, int x, int y) {
	if (x >= c->offset_x && x < (c->offset_x + c->width) &&
		y >= c->offset_y && y < (c->offset_y + c->height)) {
		return 1;
	}
	else
		return 0;
}

unsigned char canvas_get_point(struct canvas *c, int x, int y) {
	if (x < 0 || x >= c->width || y >= c->height || y < 0)
		return 0;
	return c->buffer[c->width*y+x];
}

int canvas_right_update(struct canvas *c) {
	int clip_x, clip_y, clip_size;
	int i, j;
	canvas_square_clip(c, &clip_x, &clip_y, &clip_size);
#if _DEBUG
	printf("clip: %d %d\n", clip_x, clip_y);
	printf("clip size: %d\n", clip_size);
#endif
	memset(c->buffer_input, 0U, sizeof(float)*c->gsize*c->gsize);
	if (clip_size == 0)
		return 0;
	double step = (double)clip_size / c->width;
	double x;
	double y;
	unsigned char tl, tr, br, bl;
	int input_size = c->width / c->gsize;
	for (i = 0; i < c->width; i++) {
		for (j = 0; j < c->height; j++) {
			x = i*step;
			y = j*step;
			double dx = x - (int)x;
			double dy = y - (int)y;
			tl = canvas_get_point(c, clip_x + floor(x),	clip_y + ceil(y));
			tr = canvas_get_point(c, clip_x + ceil(x), clip_y + ceil(y));
			br = canvas_get_point(c, clip_x + ceil(x), clip_y + floor(y));
			bl = canvas_get_point(c, clip_x + floor(x), clip_y + floor(y));
			double p1 = dx*tr + (1-dx)*tl;
			double p2 = dx*br + (1-dx)*bl;
			unsigned char p = (unsigned char)(dy*p1 + (1-dy)*p2);
			c->buffer_input[c->gsize*(j/input_size) + (i/input_size)] += (float)p;
		}
	}

	int size = c->width / c->gsize;
	int max = size*size*200U;
	for (i = 0; i < c->gsize; i++) {
		for (j = 0; j < c->gsize; j++) {
			c->buffer_input[i*c->gsize + j] /= max;
		}
	}
	c->render = 1;
	return 1;
}

void canvas_clipping_planes(struct canvas *c,
							int *left, int *top, int *right, int *bottom) {
	*left = c->width - 1;
	*bottom = c->height - 1;
	*right = 0;
	*top = 0;
	int i, j;
	for (i = 0; i < c->width; i++)
		for (j = 0; j < c->width; j++) {
			if (c->buffer[j*c->width + i]) {
				*left = (*left > i) ? i : *left;
				*right = (*right < i) ? i : *right;
				*top = (*top < j) ? j : *top;
				*bottom = (*bottom > j) ? j : *bottom;
			}
		}
}

void canvas_mouse_click(struct canvas *c, enum canvas_button btn,
						enum canvas_button_action action) {

	if (btn == CANVAS_BTN_LEFT) {
		if (action == CANVAS_BTN_DOWN) {
			c->btn_left_pressed = 1;
		} else {
			c->btn_left_pressed = 0;
		}
	}
}

void canvas_render(struct canvas *c) {
	glClearColor(.4, .45, .15, 0);
	glClear(GL_COLOR_BUFFER_BIT);

	glRasterPos2i(0 + c->offset_x, c->win_height - c->height - c->offset_y);
	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
	glDrawPixels(c->width, c->height, GL_RED, GL_UNSIGNED_BYTE, c->buffer);
	glPushMatrix();
	glLoadIdentity();
	glTranslatef(c->width + 10 + c->offset_x, c->win_height - c->height - c->offset_y, 0);
	int i, j;
	for (i = 0; i < c->gsize; i++) {
		for (j = 0; j < c->gsize; j++) {
			int size = c->width / c->gsize;
			float colour = (float)c->buffer_input[i*c->gsize + j];
			glColor3f(1 - colour, 1 - colour, 1 - colour);
			glBegin(GL_QUADS);
			  int basex = j*size;
			  int basey = i*size;
			  glVertex2f(basex, basey);
		      glVertex2f(basex, basey + size);
		      glVertex2f(basex + size, basey + size);
			  glVertex2f(basex + size, basey);
		    glEnd();
		}
	}
	glPopMatrix();
}

void canvas_set_point(struct canvas *c, int x, int y, unsigned char val) {
	if (x >= 0 && y >= 0 && x < c->width && y < c->height) {
		int off = x + c->width*(c->height - y - 1);
		c->buffer[off] = val;
	}
}

void canvas_cursor_move(struct canvas *c, int mx, int my) {
	c->mouse_x = mx;
	c->mouse_y = my;
	if (canvas_point_inside(c, c->mouse_x, c->mouse_y) && c->btn_left_pressed) {
		int cx = mx - c->offset_x;
		int cy = my - c->offset_y;
		int r = c->cursor_radius;
		int i, j;
		for (i = 0; i < 2*r + 1; i++)
			for (j = 0; j < 2*r + 1; j++) {
				int ax = cx - r + i;
				int ay = cy - r + j;
				int dist_x = abs(ax-cx);
				int dist_y = abs(ay-cy);
				if (dist_x*dist_x + dist_y*dist_y <= r*r - 0.005 ) {
					canvas_set_point(c, ax, ay, 200U);
				}
			}
		c->render = 1;
	}
}
